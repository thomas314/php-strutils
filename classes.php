<?php

class StrUtils {
    private $texte;

    public function __construct($str)
    {
        $this->texte = $str;
    }
    
  
    public function bold(){
        echo "<strong> $this->texte  </strong>";
    }
    public function italic(){
        echo "<i> $this->texte </i>";
    }
    public function underline(){
        echo "<u> $this->texte </u>";
    }
    public function capitalize(){
        echo "<p style='text-transform: uppercase;'> $this->texte </p>";
    }
};



?>