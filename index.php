<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>POO PHP strutils</title>
</head>
<body>
    <h1>Voici ma page d'affichage !</h1>

    <?php 
    require "classes.php";

    $str = new StrUtils('jaime les pâtes');
    ?>
    <?= $str->bold().'<br>' ?>
    <?= $str->italic().'<br>'  ?>
    <?= $str->underline().'<br>'  ?>
    <?= $str->capitalize().'<br>'  ?>

</body>
</html>